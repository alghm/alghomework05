﻿
ulong kingN = 70368760954912;
ulong kingA = kingN & 0xfefefefefefefefe;
ulong kingH = kingN & 0x7f7f7f7f7f7f7f7f;


ulong movesBits = kingA << 7 | kingN >> 8 | kingH << 9 
                | kingA >> 1              | kingH << 1 
                | kingA >> 9 | kingN << 8 | kingH >> 7;

Console.WriteLine("King bits = " + movesBits);


ulong horseN = 618475291648;
ulong horseA = 0xfefefefefefefefe;
ulong horseAB = 0xfcfcfcfcfcfcfcfc;
ulong horseH = 0x7f7f7f7f7f7f7f7f;
ulong horseGH = 0x3f3f3f3f3f3f3f3f;


movesBits = horseGH & (horseN << 6  | horseN >> 10)
          | horseH  & (horseN << 15 | horseN >> 17)
          | horseA  & (horseN << 17 | horseN >> 15)
          | horseAB & (horseN << 10 | horseN >> 6);

Console.WriteLine("Horse bits = " + movesBits);

var hpopcount1 = popcnt1(movesBits);
var hpopcount2 = popcnt2(movesBits);
Console.WriteLine("Horse bits popcnt1 = " + hpopcount1);
Console.WriteLine("Horse bits popcnt2 = " + hpopcount2);
Console.ReadKey();

static int popcnt1(ulong mask)
{
    int count = 0;
    while(mask > 0)
    {
        count += (int)mask & 1;
        mask >>= 1;
    }
     return count;
}

static int popcnt2(ulong mask)
{
    int count = 0;
    while (mask > 0)
    {
        count++;
        mask &= mask - 1;
    }
    return count;
}